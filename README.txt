-- SUMMARY --

The logmatic module uses the Logmatic.io input HTTP API to send metrics data to your logmatic account.
It's dedicated to the logmatic.io service : http://logmatic.io
Two parts in this module:
- PHP API : to collect PHP data
- RUM API : to collect Browser data. (Real User Monitoring).


For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/cutesquirrel/2660988

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2660988


-- REQUIREMENTS --

- Libraries module : https://www.drupal.org/project/libraries
- For the RUM part You have to download the Javascript libaries :
    + tracekit.js
    + logmatic-js
    + boomrang.js
    + rt.js
    + restiming.js

-- INSTALLATION --

* Install the module as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Enable the module
* Configure the permissions (one permission to allow to modify the settings)
* Go to the settings : admin/config/development/logmatic/settings
* Set your Logmatic API key : you can use the same for PHP API and RUM or 2 different ones. It's hope to you.
* Enable the PHP API if you want to collect PHP metrics/data
* Enable the RUM API if you want to load scripts to grab browser metrics : page load time, ...
   - define the pathes of the needed Javascript libraries (in this order):
      + tracekit.js
      + logmatic-js
      + boomrang.js
      + rt.js
      + restiming.js
    1. Download these libraries in your "library" path in your drupal architecture.
    2. Set the pathes in the settings : LIB_NAME|RELATIVE_PATH (one by line)
    => you can choose to give either each libraries separately or already aggregated.
* SAVE the settings.

-- API usage --

All the prefixed by "logmatic_api_" methods are used to call the API.
For the moment, only one method exists, allowing you to collect data to be sent to logmatic.

-- CONTACT --

Current maintainers:
* Etienne VOILLIOT - https://www.drupal.org/user/1480476