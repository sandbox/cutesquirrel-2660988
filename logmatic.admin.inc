<?php

/**
 * Logmatic.io HTTP API settings form.
 */
function logmatic_settings_form($form, &$form_state) {

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logmatic.io HTTP API settings'),
  );


  if (extension_loaded('curl')) {
    drupal_set_message(check_plain(t('Well done ! The PHP CURL extension is loaded')));
  }
  else {
    drupal_set_message(check_plain(t('Be careful! The PHP CURL extension has to be loaded to send metrics to logmatic.io  .')), 'error');
  }


  $form['settings']['logmatic_enable_monitoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable logmatic monitoring (PHP metrics).'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_enable_monitoring', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  $form['settings']['logmatic_enable_rum_monitoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable logmatic RUM monitoring (Browser metrics).'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_enable_rum_monitoring', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  $form['settings']['logmatic_appname'] = array(
    '#type' => 'textfield',
    '#title' => t('The appname used in your sent data by default. Can be overriden when the data are added.'),
    '#description' => t('Will be found under custom.appname in Logmatic.io.'),
    '#default_value' => variable_get('logmatic_appname', 'PHP_API'),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );


  $form['settings']['logmatic_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Your API key for logs from PHP.'),
    '#description' => t('See #configure/apikeys section in your logmatic UI.'),
    '#default_value' => variable_get('logmatic_api_key', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  $form['settings']['logmatic_rum_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Your API key for the Javascript logmatic library. Can be the same than the PHP key, it´s hope to you!'),
    '#description' => t('See #configure/apikeys section in your logmatic UI.'),
    '#default_value' => variable_get('logmatic_rum_api_key', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  $form['settings']['logmatic_php_api_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('CURL timeout: max number of seconds to send metrics to logmatic.io. If reached, an error watddog will be emityed.'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_php_api_timeout', 5),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  // -------------------------------------------------------------------
  $form['settings']['rum'] = array(
    '#type' => 'fieldset',
    '#title' => t('RUM API settings'),
  );

  $form['settings']['rum']['logmatic_rum_collect_assets'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collect assets metrics?'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_rum_collect_assets', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );
  $form['settings']['rum']['logmatic_rum_assets_time_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Collect only assets which take more than this time (in milliseconds).'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_rum_assets_time_threshold', 1000),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );


  $form['settings']['logmatic_rum_libraries'] = array(
    '#type' => 'textarea',
    '#title' => t('JS files to load before the generated inline script which collect the data.'),
    '#description' => t('You have to download all the needed libraries first : logmatic-js, boomrang.js, boomrang´s plugins (rt, restiming), optionaly tracekit.js
      <br/><strong>Format:</strong> one library by line : LIBRARY_NAME|JS_RELATIVE_PATH (ex : rum|js/rum.min.js or boomrang|boomrang.js)
      <br/>The libraries will be loaded by libraries_get_path($lib_name) . "/" . $js_path
      <br/>This system allows you to give the library in the way you want : separated files, aggregated and minified unique file, ...'),
    '#default_value' => variable_get('logmatic_rum_libraries', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  // -------------------------------------------------------------------
  $form['settings']['attributes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attributes name settings'),
  );

  $form['settings']['attributes']['logmatic_client_ip_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Client IP attribute name.'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_client_ip_field_name', 'http_client_ip'),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );
  $form['settings']['attributes']['logmatic_useragent_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('User-agent attribute name.'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_useragent_field_name', 'useragent'),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );
  $form['settings']['attributes']['logmatic_hostname_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname attribute name.'),
    '#description' => '',
    '#default_value' => variable_get('logmatic_hostname_field_name', 'hostname'),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );
  $form['settings']['attributes']['logmatic_url_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('URL attribute name. Used for the browser library only. '),
    '#description' => '',
    '#default_value' => variable_get('logmatic_url_field_name', 'url'),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );
  $form['settings']['attributes']['logmatic_unique_ID_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Optional (if not set, not used). Unique ID attribute name. (Ex: uid, uniq_id, ...). The module will try to get this uniq ID from _SERVER["UNIQUE_ID"], from the Apache unique_id module. '),
    '#description' => '',
    '#default_value' => variable_get('logmatic_unique_ID_field_name', ''),
    '#required' => FALSE,
    '#access' => array('logmatic_settings'),
  );

  return system_settings_form($form);
}