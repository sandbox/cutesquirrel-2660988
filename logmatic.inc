<?php

/**
 * Class Logmatic
 * Most of the code get from https://github.com/segmentio/analytics-php
 */
class Logmatic {

  private $options;
  private $secret;

  private $queue;
  private $max_queue_size = 1000;
  private $batch_size = 100;

  private $socket_failed;

  /**
   * Creates a new socket consumer for dispatching async requests immediately
   * @param string $secret
   * @param array  $options
   *     number   "timeout" - the timeout for connecting
   *     function "error_handler" - function called back on errors.
   *     boolean  "debug" - whether to use debug output, wait for response.
   */
  public function __construct($secret, $options = array()) {

    $this->secret = $secret;
    $this->options = $options;

    if (!isset($options["timeout"])) {
      $options["timeout"] = 5;
    }

    if (!isset($options["host"])) {
      $options["host"] = "api.logmatic.io";
    }

    if (isset($options["max_queue_size"])) {
      $this->max_queue_size = $options["max_queue_size"];
    }

    if (isset($options["batch_size"])) {
      $this->batch_size = $options["batch_size"];
    }

    $this->queue = array();
  }

  public function __destruct() {
    # Flush our queue on destruction
    $this->flush();
  }


  /**
   * Tracks a user action
   *
   * @param  array $message
   * @return boolean whether the track call succeeded
   */
  public function track(array $message) {
    return $this->enqueue($message);
  }

  /**
   * Adds an item to our queue.
   * @param  mixed $item
   * @return boolean whether the queue has room
   */
  protected function enqueue($item) {

    $count = count($this->queue);

    if ($count > $this->max_queue_size) {
      return false;
    }

    $count = array_push($this->queue, $item);

    if ($count >= $this->batch_size) {
      $this->flush();
    }

    return true;
  }


  /**
   * Flushes our queue of messages by batching them to the server
   */
  public function flush() {

    $count = count($this->queue);
    $success = true;

    while ($count > 0 && $success) {
      $batch = array_splice($this->queue, 0, min($this->batch_size, $count));
      $success = $this->flushBatch($batch);

      $count = count($this->queue);
    }

    return $success;
  }


  public function flushBatch($batch) {
    $socket = $this->createSocket();

    if (!$socket) {
      return;
    }

    //$payload = $this->payload($batch);
    $payload = json_encode($batch);

    $body = $this->createBody($this->options["host"], $payload);
    return $this->makeRequest($socket, $body);
  }


  private function createSocket() {

    if ($this->socket_failed) {
      return false;
    }

    $protocol = $this->ssl() ? "ssl" : "tcp";
    $host = $this->options["host"];
    $port = $this->ssl() ? 443 : 80;
    $timeout = $this->options["timeout"];

    try {
      # Open our socket to the API Server.
      # Since we're try catch'ing prevent PHP logs.

      $socket = @pfsockopen($protocol . "://" . $host, $port, $errno, $errstr, $timeout);

      # If we couldn't open the socket, handle the error.
      if (false === $socket) {
        $this->handleError($errno, 'createSocket: socket===false, ' . $errstr);
        $this->socket_failed = true;
        return false;
      }

      return $socket;

    }
    catch (Exception $e) {
      $this->handleError($e->getCode(), 'createSocket: exception: ' . $e->getMessage());
      $this->socket_failed = true;
      return false;
    }
  }

  /**
   * Attempt to write the request to the socket, wait for response if debug
   * mode is enabled.
   * @param  stream $socket the handle for the socket
   * @param  string $req    request body
   * @return boolean $success
   */
  private function makeRequest($socket, $req, $retry = true) {

    $bytes_written = 0;
    $bytes_total = strlen($req);
    $closed = false;

    # Write the request
    while (!$closed && $bytes_written < $bytes_total) {
      try {
        # Since we're try catch'ing prevent PHP logs.
        $written = @fwrite($socket, substr($req, $bytes_written));
      }
      catch (Exception $e) {
        $this->handleError($e->getCode(), 'makeRequest: Exception: ' . $e->getMessage());
        $closed = true;
      }
      if (!isset($written) || !$written) {
        $closed = true;
      }
      else {
        $bytes_written += $written;
      }
    }

    # If the socket has been closed, attempt to retry a single time.
    if ($closed) {
      fclose($socket);

      if ($retry) {
        $socket = $this->createSocket();
        if ($socket) {
          return $this->makeRequest($socket, $req, false);
        }
      }
      return false;
    }


    $success = true;

    if ($this->debug()) {
      $res = $this->parseResponse(fread($socket, 2048));

      if ($res["status"] != "200") {
        $this->handleError($res["status"], 'makeRequest: HTTP!=200 => ' . $res["message"]);
        $success = false;
      }
    }

    return $success;
  }


  /**
   * Create the body to send as the post request.
   * @param  string $host
   * @param  string $content
   * @return string body
   */
  private function createBody($host, $content) {

    $req = "";
    $req .= "POST /v1/input/{$this->secret} HTTP/1.1\r\n";
    $req .= "Host: " . $host . "\r\n";
    $req .= "Content-Type: application/json\r\n";
    $req .= "Accept: application/json\r\n";
    $req .= "Content-length: " . strlen($content) . "\r\n";
    $req .= "\r\n";
    $req .= $content;

    return $req;
  }


  /**
   * Parse our response from the server, check header and body.
   * @param  string $res
   * @return array
   *     string $status  HTTP code, e.g. "200"
   *     string $message JSON response from the api
   */
  private function parseResponse($res) {

    $contents = explode("\n", $res);

    # Response comes back as HTTP/1.1 200 OK
    # Final line contains HTTP response.
    $status = explode(" ", $contents[0], 3);
    $result = $contents[count($contents) - 1];

    return array(
      "status" => isset($status[1]) ? $status[1] : null,
      "message" => $result
    );
  }


  /**
   * Given a batch of messages the method returns
   * a valid payload.
   *
   * @param  {Array} batch
   * @return {Array}
   **/
  protected function payload($batch) {
    return array(
      "batch" => $batch,
      "sentAt" => date("c"),
    );
  }

  /**
   * Check whether debug mode is enabled
   * @return boolean
   */
  protected function debug() {
    return isset($this->options["debug"]) ? $this->options["debug"] : false;
  }

  /**
   * Check whether we should connect to the API using SSL. This is enabled by
   * default with connections which make batching requests. For connections
   * which can save on round-trip times, you may disable it.
   * @return boolean
   */
  protected function ssl() {
    return isset($this->options["ssl"]) ? $this->options["ssl"] : true;
  }


  /**
   * On an error, try and call the error handler, if debugging output to
   * error_log as well.
   * @param  string $code
   * @param  string $msg
   */
  protected function handleError($code, $msg) {

    if (isset($this->options['error_handler'])) {
      $handler = $this->options['error_handler'];
      $handler($code, $msg);
    }

    if ($this->debug()) {
      error_log("[Analytics][" . $this->type . "] " . $msg);
    }
  }
}
